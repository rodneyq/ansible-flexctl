# ansible-flexctl

[![License](https://img.shields.io/github/license/markosamuli/ansible-linuxbrew.svg)](https://github.com/markosamuli/ansible-linuxbrew/blob/master/LICENSE)


## Configuration

By default, the role uses Ansible to clone the Homebrew Git repository and
create all relevant directories, then install flexctl.

Target hosts are monitoring servers. As of now it needs to allow monitoring server to download from S3, 
or flexctl errors with S3 download.

```yaml
linuxbrew_use_installer: false

```

## Role Variables

Set `linuxbrew_init_shell` to `false` if you're for example managing your shell
rc files using your own .dotfiles repository.

```yaml
# Configure shell rc files
linuxbrew_init_shell: true
```

## Sample Playbook
```yaml
---
- hosts: monitor 
  vars:
    linuxbrew_home: "/opt/linuxbrew/"
  roles:
    - ansible-flexctl 
```

## Running Playbook
```bash
export ANSIBLE_TRANSPORT="ssh"
export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes"

ansible-playbook play.yml 


```
## Coding style

Install pre-commit hooks and validate coding style:

```bash
make lint
```

## Run tests

Run tests in Ubuntu and Debian using Docker:

```bash
make test
```

## License

* [BSD License](LICENSE)

## Contributions

The installation is based on the official [Homebrew installer] script.

[Homebrew installer]: https://github.com/Linuxbrew/install


