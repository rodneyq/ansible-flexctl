# Changelog

## [1.2.0] - 2019-10-25

### Changes

* Add  sample changes 


### Fixes

* Sample fixes 

## [0.0.1] - 2020-04-07

### Changes

* Use Ansible tasks to clone the Homebrew Git repositories, create directories
  and set permissions where possible instead of using the official installer
  bash script.

### Fixes

* Install installation dependencies.
* Update both `.bashrc` and `.zshrc` shell scripts.

